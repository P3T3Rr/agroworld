import React from 'react';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { hydrate,render } from 'react-dom';
import './firebase'


const root = document.getElementById("root")

if(root.hasChildNodes()){

hydrate(<App />, root)

}else{

  render(<App />, root)

}
// ReactDOM.render(
  
//    <App />,
   
//   document.getElementById('root')
// );


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
