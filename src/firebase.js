
import 'firebase/compat/firestore'
import { doc, getDoc, setDoc, updateDoc } from 'firebase/firestore'
import 'firebase/compat/auth'
import firebase from 'firebase/compat/app'
import { getAdditionalUserInfo } from 'firebase/auth'
import 'firebase/compat/storage'

const firebaseConfig = {
  apiKey: "AIzaSyAA9qV9eRgrr39gNb1IZ0xVbN-XQjFLzD0",
  authDomain: "agroworld-3c686.firebaseapp.com",
  projectId: "agroworld-3c686",
  storageBucket: "agroworld-3c686.appspot.com",
  messagingSenderId: "182554412549",
  appId: "1:182554412549:web:3c0fcf560df2b87b90fe6c"
};
firebase.initializeApp( firebaseConfig );


const facebookAuthProvider = new firebase.auth.FacebookAuthProvider()
const storage = firebase.storage().ref()
const db = firebase.firestore()

export {
  firebase,
  facebookAuthProvider,
  storage,
  db,
  getAdditionalUserInfo,
  doc,
  getDoc,
  setDoc,
  updateDoc
}

