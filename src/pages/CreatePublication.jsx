import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom"
import Swal from 'sweetalert2'
import Advertising from '../Components/MinComponents/Advertising'
import Theme from '../Components/MinComponents/Theme'
import Preview from '../Components/MinComponents/Preview'
import './Styles.css'
import { Form, CloseButton, Button } from 'react-bootstrap';
import { db, firebase } from '../firebase';
import { doc, updateDoc } from 'firebase/firestore';
import Login from '../Components/MinComponents/Login';
import Register from '../Components/MinComponents/Register';

const showAlert = () => {
    Swal.fire({
        title: "Usuario no verificado",
        text: "Debe verificar el usuario en su correo",
        icon: "error",
        confirmButtonColor: '#4285F4'
    })
};

const showAlertError = (error) => {

    if (error === "Firebase: The email address is badly formatted. (auth/invalid-email).") {
        Swal.fire({
            title: "Formato de correo inválido",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "Firebase: Password should be at least 6 characters (auth/weak-password).") {
        Swal.fire({
            title: "La contraseña debe contener mínimo 6 caracteres",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "campos vacios") {
        Swal.fire({
            title: "No debe dejar campos vacíos",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "Firebase: The email address is already in use by another account. (auth/email-already-in-use).") {
        Swal.fire({
            title: "Ya existe un usuario con ese correo",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "contraseñas") {
        Swal.fire({
            title: "Las contraseñas no coinciden",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
};

const Publication = () => {

    const init = () => {
        const userData = localStorage.getItem("usuario");
        return userData ? JSON.parse(userData) : [];
    }

    const idUsuario = init();
    const [estadoLogueado, setEstadoLogueado] = useState(false);
    const [modalOpenLogin, setModalOpenLogin] = useState(false);
    const [modalOpenRegistrer, setModalOpenRegister] = useState(false);
    const [loading, setLoading] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);
    const [newTittle, setNewTittle] = useState("");     // Titulo
    const [newBody, setNewbody] = useState("");         // Cuerpo
    const [images, setimages] = useState([]);           // Lista de imágenes de la publicación
    const [tema, setTema] = useState("Todos los temas");// Tema elegido
    const [Publicacion, setPublicacion] = useState({Titulo: "a", Cuerpo: "a"});
    const navigate = useNavigate();

    useEffect(() => {
        if (idUsuario !== "") {
            setEstadoLogueado(true)
        }
    }, []);

    const ir = () => {
        navigate('/');
    }

    const changeInput = (e) => {
        let indexImg;

        if (images.length > 0) {
            indexImg = images[images.length - 1].index + 1;
        } else {
            indexImg = 0;
        }

        let newImgsToState = readmultifiles(e, indexImg);
        let newImgsState = [...images, ...newImgsToState];
        setimages(newImgsState);
    };

    function readmultifiles(e, indexInicial) {
        const files = e.currentTarget.files;
        const arrayImages = [];

        Object.keys(files).forEach((i) => {
            const file = files[i];
            let url = URL.createObjectURL(file);

            arrayImages.push({
                index: indexInicial,
                name: file.name,
                url,
                file
            });
            indexInicial++;
        });
        return arrayImages;
    }

    function deleteImg(indice) {
        const newImgs = images.filter(function (element) {
            return element.index !== indice;
        });
        setimages(newImgs);
    }

    const Publicar = async () => {
        setLoading(true)
        try {
            let urls = []
            const fecha = firebase.firestore.Timestamp.now().toDate()
            const calificacion = [{ Calificacion: 100, Usuario: idUsuario }]
            const info = { Tema: tema, Titulo: newTittle, Cuerpo: newBody, Imagenes: [], Calificacion: calificacion, Usuario: idUsuario, Fecha: fecha };
            const nueva = await db.collection("Publicaciones").add(info);
            const idPublicacion = nueva.id

            images.forEach(async archivo => {
                const storageRef = firebase.storage().ref('Publicaciones');
                const archivoPath = storageRef.child(archivo.name);

                await archivoPath.put(archivo.file)
                await archivoPath.getDownloadURL()
                    .then(async (url) => {
                        urls.push(url);
                        await updateDoc(doc(db, "Publicaciones", idPublicacion), { Imagenes: urls });
                    });

                if (archivo.index == images.length - 1) {
                    Swal.fire({
                        title: "Publicación creada",
                        icon: "success",
                        confirmButtonColor: '#4285F4'
                    })
                    setModalOpen(false)
                    ir()
                }
            });
        } catch (err) {
            console.error(); // Evita que el error aparezca en consola
        }
    }

    const previsualizar = () => {
        if (tema !== 'Todos los temas') {
            if (newTittle !== "" && newBody !== "") {
                if (images.length !== 0) {
                    const calificacion = [{ Calificacion: 100, Usuario: idUsuario }]
                    const info = { Tema: tema, Titulo: newTittle, Cuerpo: newBody, Imagenes: [], Calificacion: calificacion, Usuario: idUsuario };
                    setPublicacion(info)
                    setModalOpen(true)
                }
                else {
                    Swal.fire({
                        title: "Seleccione al menos una imagen",
                        icon: "warning",
                        confirmButtonColor: '#4285F4'
                    })
                }
            }
            else {
                Swal.fire({
                    title: "Título y/o cuerpo vacío",
                    icon: "warning",
                    confirmButtonColor: '#4285F4'
                })
            }
        }
        else {
            Swal.fire({
                title: "Seleccione un tema",
                icon: "warning",
                confirmButtonColor: '#4285F4'
            })
        }
    }
    
    function mensajeLogin() {
        Swal.fire({
            title: "¡Inicie Sesión!",
            text: "Debe iniciar sesión para crear una publicación",
            icon: "info",
            buttons: true,
            showConfirmButton: true,
            showDenyButton: true,
            showCloseButton: true,
            denyButtonText: 'Registrame',
            confirmButtonText: 'Iniciar Sesión',
            confirmButtonColor: '#4285F4',
            denyButtonColor: '#1B1B1B'

        }).then(async (result) => {
            if (result.isConfirmed) {
                setModalOpenLogin(true)
            } else if (result.isDenied) {
                setModalOpenRegister(true)
            }
        })
    }

    return (
        <div id='body2' style={{ marginBottom: '5%' }}>
            <div className='row g-0'>
                <div className='col-9'>
                    <div className='body'>
                        <p className='title'>Crear Publicación</p>

                        <div className='row g-2'>
                            <div className='col-lg-3 col-md-4 col-12'>
                                <Theme setTema={setTema} tema={tema} />
                            </div>
                            <div className='col-lg-3 col-md-2 col-12'></div>
                            <div className='col-lg-6 col-md-6 col-12'>
                                <Form.Control type='text' placeholder='Insertar Título' size='lg' onChange={(event) => { setNewTittle(event.target.value) }}></Form.Control>
                            </div>
                        </div>

                        <div id='textarea'>
                            <textarea name="cuerpo" rows="10" style={{ width: '100%' }} onChange={(event) => { setNewbody(event.target.value) }}></textarea>
                        </div>

                        <div className='row g-2'>
                            <div className='d-grid gap-1 col-12 col-md-5 col-lg-4'>
                                <label className="btn btn-outline-dark btn-lg">
                                    <span>Seleccionar Fotos</span>
                                    <input hidden type="file" accept='image/*' multiple onChange={changeInput}></input>
                                </label>
                            </div>
                            <div className=' col-12 col-md-2 col-lg-4'>

                            </div>
                            {estadoLogueado ? (
                            <div className='d-grid gap-1 col-12 col-md-5 col-lg-4'>
                                <Button size="lg" variant="primary" onClick={previsualizar}>Publicar</Button>
                            </div>
                            ) : (
                                <div id='botonDesabilitado' className='d-grid gap-1 col-12 col-md-5 col-lg-4'>
                                <Button size="lg" variant="primary" onClick={mensajeLogin}>Publicar</Button>
                            </div>
                                )}
                        </div>

                        <div className="row g-0">
                            <div className='row g-0 col-12 col-md-6'>
                                {images.map((imagen) => (
                                    <div className="col-1" key={imagen.index} style={{marginRight: '5rem' }}>
                                        <div id='content_img'>
                                            <CloseButton onClick={deleteImg.bind(this, imagen.index)} className="position-absolute bg-danger p-1 " />
                                            <img alt="algo" src={imagen.url} data-toggle="modal" data-target="#ModalPreViewImg" id='imagen'/>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>

                <div className='col-3' style={{ borderLeft: "2px solid black" }}>
                    <Advertising />
                </div>
            </div>
            <Preview 
                Abierto={modalOpen}
                Cerrar={setModalOpen}
                Publicacion={Publicacion}
                Imagenes={images}
                Publicar={Publicar}
                Loading={loading}
            >
            </Preview>
            <Login
                abierto={modalOpenLogin}
                setAbrir={setModalOpenLogin}
                showAlert={showAlert}
                setModalOpenLogin={setModalOpenLogin}
                setEstadoLogueado={setEstadoLogueado}>
            </Login>
            <Register
                abierto={modalOpenRegistrer}
                setModalOpenRegister={setModalOpenRegister}
                showAlertError={showAlertError}>
            </Register>
        </div>
    );
}


export default Publication;