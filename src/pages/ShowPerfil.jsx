import React, { useState, useEffect } from 'react';
import Advertising from '../Components/MinComponents/Advertising';
import { useParams } from "react-router-dom";
import { db } from "../firebase";
import { Button, Carousel } from 'react-bootstrap';
import './Styles.css'
import { getDocs, collection, query, orderBy } from 'firebase/firestore';
import { async } from '@firebase/util';

const styles = {
    correo: {
        marginTop: "20px",
        fontSize: "large"
    },
    link: {
        marginTop: "20px",
        fontSize: "large"
    },
}

const Publication = () => {

    const IdUsuario = useParams().id;
    const [Usuario, setUsuario] = useState({ nombre: "", foto: "" });
    const [Publicaciones, setPublicaciones] = useState([]);
    const [tienePublicaciones, setTienePublicaciones] = useState(false);

    useEffect(async () => {
        await cargarUsuario()
    }, []);

    const cargarUsuario = async () => {
        const snap = await getDocs(collection(db, "usuarios"))
        let usuario;

        snap.forEach((doc) => {
            if (doc.id === IdUsuario) {
                usuario = doc.data();
            }
        });
        setUsuario(usuario);
        await cargarPublicaciones()
    }

    const cargarPublicaciones = async () => {
        const data = await getDocs(query(collection(db, "Publicaciones"), orderBy("Fecha", "desc")))

        let publicaciones = [];

        data.forEach((doc) => {
            if (doc.data().Usuario === IdUsuario) {
                publicaciones.push([doc.id, doc.data()]);
                setTienePublicaciones(true)
            }
        });
        setPublicaciones(publicaciones);
    }

    return (
        <div id='body2' style={{ marginBottom: '5%' }}>
            <div className='row g-0'>
                <div className='col-9'>
                    <div className='body'>
                        <div id='datosUsuarios' className='row g-0 justify-content-center'>
                            <div id='divFoto3' className='col-12 col-md-5'>
                                <img id='imagen' src={Usuario.foto} alt="fotoUsuario" />
                            </div>
                            <div className='col-12 col-md-7' style={{ textAlign: 'center', alignSelf: 'center' }}>
                                <div>
                                    <h2>{Usuario.nombre}</h2>
                                    <p style={styles.correo}>{Usuario.correo}</p>
                                    <a href={Usuario.Link} style={styles.link}>{Usuario.Link}</a><br />
                                    <Button style={{ marginTop: '20px' }} className='col-8' variant="outline-danger"><i class="fa-solid fa-user-slash"></i> Reportar</Button>
                                </div>
                            </div>
                        </div>

                        <div className='row g-0 justify-content-center'>
                            <div className='col-9' style={{ marginTop: '50px' }}>

                                <p style={{ fontSize: 'x-large' }}>Publicaciones</p>
                                <Carousel>
                                    {Publicaciones.map((publicacion) => (
                                        <Carousel.Item>
                                            <a href={'/publication/' + publicacion[0]}>
                                                <div id='carruselPublicaciones'>
                                                    <a href={'/publication/' + publicacion[0]}>
                                                        <div id='fondo'>
                                                            <img id='imagen' src={publicacion[1].Imagenes[0]} alt="" />
                                                        </div>
                                                    </a>
                                                </div>
                                                <Carousel.Caption>
                                                    <h1>{publicacion[1].Titulo}</h1>
                                                    <p style={{ fontSize: 'x-large' }}>Tema: {publicacion[1].Tema}</p>
                                                </Carousel.Caption>
                                            </a>
                                        </Carousel.Item>
                                    ))}
                                </Carousel>
                                {tienePublicaciones ? (
                                    <></>
                                ) : (
                                    <p style={{ fontSize: 'x-large', textAlign: 'center' }}>Este usuario no ha hecho ninguna publicación</p>
                                )
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div className='col-3' style={{ borderLeft: "2px solid black" }}>
                    <Advertising />
                </div>
            </div>
        </div>
    );
}


export default Publication;