import React, { useState, useEffect } from 'react';
import { Carousel, Alert } from 'react-bootstrap';
import Slider from '../Components/MinComponents/Slider';
import Advertising from '../Components/MinComponents/Advertising';
import { useParams } from "react-router-dom";
import './Styles.css';
import Comments from '../Components/Comments';
import { db, firebase } from "../firebase";
import { getDocs, collection, orderBy, query } from 'firebase/firestore';
import { useNavigate } from "react-router-dom"

const styles = {
    input: {
        width: "100%",
        height: "40px",
    },
    button: {
        width: "100%",
        height: "40px",
        backgroundColor: "#202020",
        color: "#ffffff"
    },
    divCalificarPublicacion: {
        alignSelf: "center",
        textAlign: "center",
        fontSize: "large",
        marginTop: "30px"
    }
}

const Publication = () => {

    const init = () => {
        const userData = localStorage.getItem("usuario");
        return userData ? JSON.parse(userData) : [];
    }

    const [estadoLogueado, setEstadoLogueado] = useState(false);
    const id = useParams().id;
    const [publication, setPublication] = useState([]);
    const [comentarios, setComentarios] = useState([]);
    const idUsuario = init();
    const [comen, setVal] = useState();
    const [Usuario, setUsuario] = useState([]);
    const [cambio, setCambio] = useState(true);
    const [usuarioCreador, setCreador] = useState({ nombre: "", foto: "" });
    const [idUsuarioCreador, setIdUsuarioCreador] = useState("");
    
    const navigate = useNavigate();
    function verPerfil() {
        navigate('/user/' + idUsuarioCreador)
    }

    useEffect(() => {
        if (idUsuario !== "") {
            setEstadoLogueado(true)
        }

        cargarData()
        cargarComentarios()
        cargarUsuario()
    }, []);

    const cargarData = async () => {
        const snap = await getDocs(collection(db, "Publicaciones"))
        const publica = [];

        snap.forEach(async (doc) => {
            if (doc.id === id) {
                publica.push({ ...doc.data() });
                await cargarUsuarioCreador(doc.data().Usuario)
            }
        });

        setPublication(publica);
    }

    const cargarUsuario = async () => {
        const snap = await getDocs(collection(db, "usuarios"))
        const usuario = [];

        snap.forEach((doc) => {
            if (doc.id === idUsuario) {
                usuario.push({ ...doc.data() });
            }
        });
        setUsuario(usuario);
    }

    const cargarUsuarioCreador = async (id) => {
        const snap = await getDocs(collection(db, "usuarios"))
        let usuario;
        let idCreador;

        snap.forEach((doc) => {
            if (doc.id === id) {
                usuario = doc.data();
                idCreador = doc.id;
            }
        });
        setIdUsuarioCreador(idCreador)
        setCreador(usuario);
    }

    const cargarComentarios = async () => {

        const snap = await getDocs(query(collection(db, "Comentarios"), orderBy("Fecha", "desc")))
        const snapUs = await getDocs(collection(db, "usuarios"))

        setComentarios([])
        const comentario = [];
        snap.forEach((doc) => {
            let coment = { ...doc.data() }
            let publicacion = coment.Publicacion
            if (publicacion === id) {
                snapUs.forEach((docU) => {
                    if (docU.id === coment.Usuario) {
                        comentario.push([docU.id, coment, { ...docU.data() }, doc.id]);
                    }
                });
            }
        });
        setComentarios(comentario);

    }

    const comentar = async () => {
        const comentario = document.getElementById("nuevoComentario").value
        const calificacion = [{ Calificacion: 100, Usuario: idUsuario }]
        if (comentario !== "") {
            const fecha = firebase.firestore.Timestamp.now().toDate()
            const comment = { Calificacion: calificacion, Comentario: comentario, Fecha: fecha, Publicacion: id, Usuario: idUsuario };
            await db.collection("Comentarios").add(comment);

            cargarComentarios()
            cargarData()
            cargarUsuario()
            setVal("")
            setCambio(!cambio)
        }
    }

    return (
        <div id='body2'>
            <div className="row g-0">
                {publication.map((publicacion, index) =>
                    <div key={index} className="col-9">
                        <div className='body'>
                            <p className='title'>{publicacion.Titulo}</p>
                            <p>{publicacion.Cuerpo}</p>

                            <div className="row g-0">
                                <div className="col-lg-7 col-md-5">
                                    <Carousel>
                                        {publicacion.Imagenes.map((image) => (
                                            <Carousel.Item>
                                                <div id='divImagenCarrusel'>
                                                    <a href={image}>
                                                        <img id='imagen' src={image} alt={image.nombre} />
                                                    </a>
                                                </div>
                                            </Carousel.Item>
                                        ))}
                                    </Carousel>
                                </div>


                                <div style={styles.divCalificarPublicacion} className="col-lg-5 col-md-7">
                                    <p>¿Qué te pareció la publicación?</p>
                                    {estadoLogueado ? (
                                        <Slider calificaciones={publicacion.Calificacion} idSlider={id} usuario={idUsuario} componente={'P'} />
                                    ) : (
                                        <div id="desabilitado">
                                            <Slider calificaciones={publicacion.Calificacion} idSlider={id} usuario={idUsuario} componente={'P'} />
                                        </div>
                                    )}

                                    <div>
                                        <br />
                                        <p>Publicado por:</p>
                                        <div className='row g-0 justify-content-center'>
                                            <div id='divFoto2'>
                                                <img onClick={verPerfil} id='imagen' src={usuarioCreador.foto} alt="foto del creador" />
                                            </div>
                                            <div>
                                                <abbr id='nombre' onClick={verPerfil} style={{ margin: "0" }} title="Ver perfil del usuario">{usuarioCreador.nombre}</abbr>
                                                <p id="correo">{usuarioCreador.correo}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <p className='title'>Comentarios</p>
                                {estadoLogueado ? (
                                    <div className="row g-0">
                                        {Usuario.map((usuario, index) =>
                                            <div key={index + usuario} id='divFoto' className="col-12 col-lg-1">
                                                <img id='imagen' src={usuario.foto} alt="Foto del Usuario" />
                                            </div>
                                        )}
                                        <div className="row g-3 col-12 col-lg-11">
                                            <div className="col-12 col-md-9">
                                                <input placeholder=" Comentario" style={styles.input} onChange={(e) => setVal(e.target.value)} value={comen} type="text" id='nuevoComentario' />
                                            </div>
                                            <div className="col-12 col-md-3">
                                                <button style={styles.button} onClick={comentar}>Comentar</button>
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <Alert variant='danger' ><p id='mensaje'>Inicie sesión para comentar, responder y/o calificar</p></Alert>
                                )}
                                {comentarios.map((comentario, index) => (
                                    <Comments comentarios={comentarios} setComentarios={setComentarios} comentario={comentario} index={index} idComentario={comentario[3]} />
                                ))}
                            </div>
                        </div>
                    </div>
                )}

                <div className="col-3" style={{ borderLeft: "2px solid black", minHeight: "100%" }}>
                    <Advertising />
                </div>

            </div>
        </div>
    )
}

export default Publication