import React, { useState, useEffect } from 'react';
import Advertising from '../Components/MinComponents/Advertising';
import { useParams } from "react-router-dom";
import { db } from "../firebase";
import { Button, Carousel } from 'react-bootstrap';
import './Styles.css'
import { getDocs, collection, query, orderBy, deleteDoc, doc } from 'firebase/firestore';
import Swal from 'sweetalert2'

const ShowPublication = () => {

    const IdUsuario = useParams().id;
    const [Publicaciones, setPublicaciones] = useState([]);
    const [tienePublicaciones, setTienePublicaciones] = useState(false);

    useEffect(async () => {
        await cargarPublicaciones()
    }, []);

    const cargarPublicaciones = async () => {
        const data = await getDocs(query(collection(db, "Publicaciones"), orderBy("Fecha", "desc")))

        let publicaciones = [];

        data.forEach((doc) => {
            if (doc.data().Usuario === IdUsuario) {
                publicaciones.push([doc.id, doc.data()]);
                setTienePublicaciones(true)
            }
        });
        setPublicaciones(publicaciones);
    }

    function eliminar(id) {
        Swal.fire({
            title: "¿Seguro que desea eliminar esta publicación?",
            icon: "question",
            buttons: true,
            showConfirmButton: true,
            showDenyButton: true,
            denyButtonText: 'Cancelar',
            confirmButtonText: 'Eliminar',
            confirmButtonColor: '#4285F4',

        }).then(async (result) => {
            if (result.isConfirmed) {
                await deleteDoc(doc(db, "Publicaciones", id));
                Swal.fire({
                    title: '¡Publicación Eliminada!',
                    icon: 'success',
                    confirmButtonColor: '#4285F4'
                }).then(() => {
                    cargarPublicaciones()
                })
            } else if (result.isDenied) {
                Swal.fire({
                    title: '¡No se eliminó!',
                    icon: 'error',
                    confirmButtonColor: '#4285F4'
                })
            }
        })
    }

    return (
        <div id='body2' style={{ marginBottom: '5%' }}>
            <div className='row g-0'>
                <div className='col-9'>
                    <div className='body'>
                        <p className='title'>Gestionar Publicaciones</p>
                    </div>

                    <div className='row g-0 justify-content-center'>
                        <div className='col-9'>

                            <p style={{ fontSize: 'x-large' }}>Mis publicaciones</p>
                            <Carousel>
                                {Publicaciones.map((publicacion) => (
                                    <Carousel.Item>
                                        <div id='carruselPublicaciones'>
                                            <div id='fondo'>
                                                <img id='imagen' src={publicacion[1].Imagenes[0]} alt="" />
                                            </div>
                                        </div>
                                        <Carousel.Caption>
                                            <h1>{publicacion[1].Titulo}</h1>
                                            <p style={{ fontSize: 'x-large' }}>Tema: {publicacion[1].Tema}</p>
                                            <Button id='botonPequeño' href={'/publication/' + publicacion[0]} size="lg" variant="light" ><i class="fa-solid fa-eye"></i></Button>
                                            <Button id='botonPequeño' href={'/editPublication/' + publicacion[0]} size="lg" variant="primary" ><i class="fa-solid fa-file-signature"></i></Button>
                                            <Button id='botonPequeño' onClick={eliminar.bind(this, publicacion[0])} size="lg" variant="danger" ><i class="fa-solid fa-trash-can"></i></Button>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                ))}
                            </Carousel>
                            {tienePublicaciones ? (
                                <></>
                            ) : (
                                <p style={{ fontSize: 'x-large', textAlign: 'center' }}>Este usuario no ha hecho ninguna publicación</p>
                            )
                            }
                        </div>
                    </div>
                </div>

                <div className='col-3' style={{ borderLeft: "2px solid black" }}>
                    <Advertising />
                </div>
            </div>
        </div>
    );
}


export default ShowPublication;