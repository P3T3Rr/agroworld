import React, { useState, useEffect } from 'react';
import Advertising from '../Components/MinComponents/Advertising';
import { useParams } from "react-router-dom";
import Theme from '../Components/MinComponents/Theme'
import Preview from '../Components/MinComponents/Preview'
import './Styles.css'
import { Form, CloseButton, Button } from 'react-bootstrap';
import { getDocs, collection, query, orderBy } from 'firebase/firestore';
import { db, firebase } from '../firebase';
import { doc, updateDoc } from 'firebase/firestore';
import Swal from 'sweetalert2'
import { useNavigate } from "react-router-dom"

const Edit = () => {

    const init = () => {
        const userData = localStorage.getItem("usuario");
        return userData ? JSON.parse(userData) : [];
    }

    const IdPublicacion = useParams().id;
    const idUsuario = init();
    const [estadoLogueado, setEstadoLogueado] = useState(false);
    const [Publicacion, setPublicacion] = useState([]);
    const [loading, setLoading] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);
    const [newTittle, setNewTittle] = useState("");     // Titulo
    const [newBody, setNewbody] = useState("");         // Cuerpo
    const [images, setimages] = useState([]);           // Lista de imágenes de la publicación
    const [tema, setTema] = useState("Todos los temas");// Tema elegido
    const navigate = useNavigate();

    useEffect(async () => {
        if (idUsuario !== "") {
            setEstadoLogueado(true)
        }
        await cargarPublicacion()
    }, []);

    const ir = () => {
        navigate('/');
    }

    const cargarPublicacion = async () => {
        const snap = await getDocs(collection(db, "Publicaciones"))
        let publicacion;
        let imagenes = []

        snap.forEach((doc) => {
            if (doc.id === IdPublicacion) {
                publicacion = [doc.id, doc.data()];
                setNewTittle(doc.data().Titulo)
                setNewbody(doc.data().Cuerpo)
                setTema(doc.data().Tema)
                doc.data().Imagenes.forEach((imagen, index) => {
                    imagenes.push({
                        index: index,
                        url: imagen
                    })
                })
                console.log("viejas", imagenes)
                setimages(imagenes)
            }
        });
        setPublicacion(publicacion);
    }

    const changeInput = (e) => {
        let indexImg;

        if (images.length > 0) {
            indexImg = images[images.length - 1].index + 1;
        } else {
            indexImg = 0;
        }

        let newImgsToState = readmultifiles(e, indexImg);
        let newImgsState = [...images, ...newImgsToState];
        setimages(newImgsState);
        console.log("nueva: ", newImgsState)
    };

    function readmultifiles(e, indexInicial) {
        const files = e.currentTarget.files;
        const arrayImages = [];

        Object.keys(files).forEach((i) => {
            const file = files[i];
            let url = URL.createObjectURL(file);

            arrayImages.push({
                index: indexInicial,
                name: file.name,
                url,
                file
            });
            indexInicial++;
        });
        return arrayImages;
    }

    function deleteImg(indice) {
        const newImgs = images.filter(function (element) {
            return element.index !== indice;
        });
        setimages(newImgs);
    }

    const Publicar = async () => {
        setLoading(true)
        try {
            let urls = []

            const fecha = firebase.firestore.Timestamp.now().toDate()
            const calificacion = [{ Calificacion: 100, Usuario: idUsuario }]

            images.forEach(async archivo => {
                try{
                    const storageRef = firebase.storage().ref('Publicaciones');
                    const archivoPath = storageRef.child(archivo.name);
                    await archivoPath.put(archivo.file)
                    await archivoPath.getDownloadURL()
                        .then(async (url) => {
                            urls.push(url);
                            await updateDoc(doc(db, "Publicaciones", IdPublicacion), { Imagenes: urls });
                        });    
                }
                catch (e){
                    urls.push(archivo.url)
                    await updateDoc(doc(db, "Publicaciones", IdPublicacion), { Imagenes: urls });
                }
                
                
                if (archivo.index == images.length - 1) {
                    
                    await updateDoc(doc(db, "Publicaciones", IdPublicacion), { Tema: tema, Titulo: newTittle, Cuerpo: newBody, Calificacion: calificacion, Usuario: idUsuario, Fecha: fecha });
                    
                    Swal.fire({
                        title: "Publicación actualizada",
                        icon: "success",
                        confirmButtonColor: '#4285F4'
                    })
                    setModalOpen(false)
                    ir()
                }
            });


        } catch (err) {
            console.error(); // Evita que el error aparezca en consola
        }
    }

    const previsualizar = () => {
        if (tema !== 'Todos los temas') {
            if (newTittle !== "" && newBody !== "") {
                if (images.length !== 0) {
                    const calificacion = [{ Calificacion: 100, Usuario: idUsuario }]
                    const info = { Tema: tema, Titulo: newTittle, Cuerpo: newBody, Imagenes: [], Calificacion: calificacion, Usuario: idUsuario };
                    setPublicacion(info)
                    setModalOpen(true)
                }
                else {
                    Swal.fire({
                        title: "Seleccione al menos una imagen",
                        icon: "warning",
                        confirmButtonColor: '#4285F4'
                    })
                }
            }
            else {
                Swal.fire({
                    title: "Título y/o cuerpo vacío",
                    icon: "warning",
                    confirmButtonColor: '#4285F4'
                })
            }
        }
        else {
            Swal.fire({
                title: "Seleccione un tema",
                icon: "warning",
                confirmButtonColor: '#4285F4'
            })
        }
    }

    const mensajeLogin = () => {
        Swal.fire({
            title: "¡Inicie Sesión!",
            text: "Debe iniciar sesión para editar una una publicación",
            icon: "warning",
            confirmButtonColor: '#4285F4'
        })
    }


    return (
        <div id='body2' style={{ marginBottom: '5%' }}>
            <div className='row g-0'>
                <div className='col-9'>
                    <div className='body'>
                        <p className='title'>Editar Publicación</p>

                        <div className='row g-2'>
                            <div className='col-lg-3 col-md-4 col-12'>
                                <Theme setTema={setTema} tema={tema} />
                            </div>
                            <div className='col-lg-3 col-md-2 col-12'></div>
                            <div className='col-lg-6 col-md-6 col-12'>
                                <Form.Control type='text' defaultValue={newTittle} placeholder='Insertar Título' size='lg' onChange={(event) => { setNewTittle(event.target.value) }}></Form.Control>
                            </div>
                        </div>

                        <div id='textarea'>
                            <textarea defaultValue={newBody} name="cuerpo" rows="10" style={{ width: '100%' }} onChange={(event) => { setNewbody(event.target.value) }}></textarea>
                        </div>

                        <div className='row g-2'>
                            <div className='d-grid gap-1 col-12 col-md-5 col-lg-4'>
                                <label className="btn btn-outline-dark btn-lg">
                                    <span>Seleccionar Fotos</span>
                                    <input hidden type="file" accept='image/*' multiple onChange={changeInput}></input>
                                </label>
                            </div>
                            <div className=' col-12 col-md-2 col-lg-4'>

                            </div>
                            {estadoLogueado ? (
                                <div className='d-grid gap-1 col-12 col-md-5 col-lg-4'>
                                    <Button size="lg" variant="primary" onClick={previsualizar}>Publicar</Button>
                                </div>
                            ) : (
                                <div id='botonDesabilitado' className='d-grid gap-1 col-12 col-md-5 col-lg-4'>
                                    <Button size="lg" variant="primary" onClick={mensajeLogin}>Publicar</Button>
                                </div>
                            )}
                        </div>
                        <div className="row g-0">
                            <div className='row g-0 col-12 col-md-6'>
                                {images.map((imagen) => (
                                    <div className="col-1" style={{ marginRight: '5rem' }}>
                                        <div id='content_img'>
                                            <CloseButton onClick={deleteImg.bind(this, imagen.index)} className="position-absolute bg-danger p-1 " />
                                            <img alt="algo" src={imagen.url} data-toggle="modal" data-target="#ModalPreViewImg" id='imagen' />
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>

                <div className='col-3' style={{ borderLeft: "2px solid black" }}>
                    <Advertising />
                </div>

                <Preview
                    Abierto={modalOpen}
                    Cerrar={setModalOpen}
                    Publicacion={Publicacion}
                    Imagenes={images}
                    Publicar={Publicar}
                    Loading={loading}
                >
                </Preview>
            </div>
        </div>
    );
}


export default Edit;