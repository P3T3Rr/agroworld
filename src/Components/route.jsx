import React,{lazy,Suspense} from "react";
import { BrowserRouter, Routes, Route} from "react-router-dom";
import Header from "./Header";
const Search = lazy(()=>import ( "././Search") );
const Publication = lazy(()=>import ( "../pages/Publication") );
const CreatePublication = lazy(()=>import ( "../pages/CreatePublication") );
const ShowPerfil = lazy(()=>import ( "../pages/ShowPerfil") );
const EditPublication = lazy(()=>import ( "../pages/EditPublication") );
const ShowPublications = lazy(()=>import ( "../pages/ShowPublications") );

const Enrrutamiento = () => {
    return (
        <BrowserRouter>
        <Suspense  fallback={<h1 style ={{marginTop : "250px" , textAlign: "center" }} >Cargando...</h1>}>
            <Header/>
            <Routes>
                
                <Route path="/" element={<Search/>} /> 
                <Route path="/publication/:id" element={<Publication/>} />
                <Route path="/createPublication" element={<CreatePublication/>}/>
                <Route path="/user/:id" element={<ShowPerfil/>}/>
                <Route path="/editPublication/:id" element={<EditPublication/>}/>
                <Route path="/showPublications/:id" element={<ShowPublications/>}/>
              
            </Routes>
            </Suspense>

        </BrowserRouter>
    
    );
  }
  
  export default Enrrutamiento;