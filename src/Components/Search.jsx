import React, { useState, useEffect } from 'react';
import Publications from './MinComponents/Publications';
import { useNavigate } from "react-router-dom"
import { Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import Theme from './MinComponents/Theme'

const styles = {
    FormStyle: {
        marginLeft: "5%",
        marginRight: "5%"
    },

    SearhForm: {
        display: "grid",
        gridTemplateColumns: "auto 9rem"
    },

    SearchButton: {
        marginLeft: '10%'
    }
}

const Search = () => {

    const [listaCompleta, setListaCompleta] = useState([]);
    const [listaOriginal, setListaOriginal] = useState([]);
    const [listaAux, setListAux] = useState([])
    const [listaAuxBuscada, setListaAuxBuscada] = useState([])
    const [tema, setTema] = useState("Todos los temas")
    const navigate = useNavigate();
    let filtro = "";

    const vaciar = () => {
        document.getElementById("filtro").value = ""
    }

    const ir = () => {
        navigate('/createPublication');
    }

    const filtrar = () => {
        filtro = document.getElementById("filtro").value
        const nueva = [];
        if (tema !== "Todos los temas") {

            for (let index = 0; index < listaAuxBuscada.length; index++) {
                let titulo = listaAuxBuscada[index].Titulo.toLowerCase()
                let cuerpo = listaAuxBuscada[index].Cuerpo.toLowerCase()
                let i = titulo.indexOf(filtro.toLowerCase())
                let j = cuerpo.indexOf(filtro.toLowerCase())

                if (i >= 0 || j >= 0) {
                    nueva.push(listaAuxBuscada[index]);
                }
            }
            setListaOriginal(nueva)
        } else {
            if (filtro !== '') {

                for (let index = 0; index < listaAux.length; index++) {
                    let titulo = listaAux[index].Titulo.toLowerCase()
                    let cuerpo = listaAux[index].Cuerpo.toLowerCase()
                    let i = titulo.indexOf(filtro.toLowerCase())
                    let j = cuerpo.indexOf(filtro.toLowerCase())

                    if (i >= 0 || j >= 0) {
                        nueva.push(listaAux[index]);
                    }
                }
                setListaOriginal(nueva)

            } else {
                setListaOriginal(listaCompleta)

            }
        }

    }

    function pulsar(e) {
        if (e.code === "NumpadEnter" || e.code === "Enter") {
            e.preventDefault();
            filtrar();
        }
    }

    const onChange = () => {
        filtro = document.getElementById("filtro").value
        if (filtro === "") {
            filtrar()
        }
    }

    return (
        <div id='body'>
            <div style={{ marginTop: '5%', marginBottom: '5%' }}>
                <form className="row g-5" style={styles.FormStyle}>
                    <div className="d-grid gap-1 col-xl-2 col-md-4">
                        <Theme setTema={setTema} tema={tema} />
                    </div>

                    <div className="d-grid gap-2 justify-content-md-center col-xl-7 col-md-8">
                        <div className='row g-3'>
                            <div className="d-grid gap-1 col-md-8">
                                <Form.Control onChange={onChange} size="lg" type="text" placeholder="Buscar Publicación" id='filtro' onKeyPress={pulsar}></Form.Control>
                            </div>
                            <div className="d-grid gap-1 col-md-4">
                                <Button onClick={filtrar} size="lg" variant="dark" ><FontAwesomeIcon icon={faMagnifyingGlass} /> Buscar</Button>
                            </div>
                        </div>
                    </div>

                    <div className="d-grid gap-1 col-xl-3">
                        <Button onClick={ir} size="lg"><FontAwesomeIcon icon={faPlus} /> Crear Publicación</Button>
                    </div>
                </form>
            </div>

            <div>
                <p style={{ marginLeft: '7%', fontSize: 'large' }}>{listaOriginal.length} publicaiones encontradas</p>
            </div>

            <div style={{ marginLeft: '5%', marginRight: '5%' }}>
                <Publications tema={tema} setListaOriginal={setListaOriginal}
                    listaOriginal={listaOriginal}
                    setListAux={setListAux} listaAux={listaAux}
                    setListaAuxBuscada={setListaAuxBuscada}
                    setListaCompleta={setListaCompleta}
                    vaciar={vaciar}
                />
            </div>
        </div>
    )
}

export default Search;