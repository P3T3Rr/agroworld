import React, { useState, useEffect } from 'react';
import Slider from './MinComponents/Slider';
import { db } from "../firebase"
import { getDocs, collection } from 'firebase/firestore';
import { useNavigate } from "react-router-dom"

const styles = {
    title: {
        marginTop: "50px",
        marginBottom: "25px",
        textAlign: "center",
        fontSize: "xx-large",
    },
    body: {
        textAlign: "justify",
        marginLeft: "5%",
        marginRight: "5%"
    },
    divComentario: {
        marginTop: "40px"
    },
    divUser: {
        display: "flex",
        justifyContent: "center"
    },
    cuerpo: {
        marginTop: "20px"
    },
    userName: {
        marginTop: "auto",
        alignSelf: "center",
        fontWeight: "bold",
        fontSize: "large",
        cursor: 'pointer'
    },
    divFoto: {
        width: "4rem",
        height: "4rem",
        overflow: "hidden",
        marginTop: "15px",
        marginRight: "5px",
        borderRadius: "100%",
        cursor: 'pointer'
    },
    input: {
        width: "100%",
        height: "40px",
    },
    button: {
        width: "100%",
        height: "40px",
        backgroundColor: "#202020",
        color: "#ffffff"
    }
}

const Response = ({respuesta}) => {

    const init = () => {
        const userData = localStorage.getItem("usuario");
        return userData ? JSON.parse(userData) : [];
    }

    const [estadoLogueado, setEstadoLogueado] = useState(false);
    const idUsuario = init();
    const [idUsuarioCreador, setIdUsuarioCreador] = useState("");

    const navigate = useNavigate();
    function verPerfil() {
        navigate('/user/' + idUsuarioCreador)
    }

    useEffect(() => {
        if (idUsuario !== "") {
            setEstadoLogueado(true)
        }
        cargarUsuarioCreador(respuesta[0])
    }, []);

    const cargarUsuarioCreador = async (id) => {
        const snap = await getDocs(collection(db, "usuarios"))
        let usuario;
        let idCreador;

        snap.forEach((doc) => {
            if (doc.id === id) {
                usuario = doc.data();
                idCreador = doc.id;
            }
        });
        setIdUsuarioCreador(idCreador)
    }

    return (
        <div>
            <div style={styles.divComentario} className="row g-0">
                <div style={styles.divUser} className="row g-0 col-md-2">
                    <div style={styles.divFoto} className="col-6 col-md-12">
                        <img onClick={verPerfil} id='imagen' src={respuesta[2].foto} alt="Foto del Usuario" />
                    </div>
                </div>

                <div style={styles.cuerpo} className="col-md-10">
                    <p onClick={verPerfil} style={styles.userName}>{respuesta[2].nombre}</p>
                    <p>{respuesta[1].Respuesta}</p>
                    {estadoLogueado ? (
                        <Slider key={respuesta[3]} calificaciones={respuesta[1].Calificacion} idSlider={respuesta[3]} usuario={idUsuario} componente={'R'} />
                    ) : (
                        <div id='desabilitado'>
                            <Slider key={respuesta[3]} calificaciones={respuesta[1].Calificacion} idSlider={respuesta[3]} usuario={idUsuario} componente={'R'} />
                        </div>
                    )}
                </div>
            </div>
        </div>
    )
}

export default Response

