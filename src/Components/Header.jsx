import Swal from 'sweetalert2'
import { db } from "../firebase"
import { collection, getDocs } from "firebase/firestore";
import React, { useState, useEffect } from 'react';
import icono from '../Imagenes/mundo.png';
import logo from '../Imagenes/logo.png';
import Login from "./MinComponents/Login";
import Register from "./MinComponents/Register";
import EditAccount from "./MinComponents/EditAccount";
import "./Header.css"
import { Link } from "react-router-dom"
import { useNavigate } from "react-router-dom"


const showAlert = () => {
    Swal.fire({
        title: "Usuario no verificado",
        text: "Debe verificar el usuario en su correo",
        icon: "error",
        confirmButtonColor: '#4285F4'
    })
};

const showAlertError = (error) => {

    if (error === "Firebase: The email address is badly formatted. (auth/invalid-email).") {
        Swal.fire({
            title: "Formato de correo inválido",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "Firebase: Password should be at least 6 characters (auth/weak-password).") {
        Swal.fire({
            title: "La contraseña debe contener mínimo 6 caracteres",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "campos vacios") {
        Swal.fire({
            title: "No debe dejar campos vacíos",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "Firebase: The email address is already in use by another account. (auth/email-already-in-use).") {
        Swal.fire({
            title: "Ya existe un usuario con ese correo",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
    if (error === "contraseñas") {
        Swal.fire({
            title: "Las contraseñas no coinciden",
            icon: "error",
            confirmButtonColor: '#4285F4'
        })
    };
};

const Header = () => {

    const init = () => {
        const userData = localStorage.getItem("usuario");
        return userData ? JSON.parse(userData) : [];
    }

    const [modalOpenLogin, setModalOpenLogin] = useState(false);
    const [modalOpenRegistrer, setModalOpenRegister] = useState(false);
    const [modalOpenAccount, setModalOpenAccount] = useState(false);
    const [estadoLogueado, setEstadoLogueado] = useState(true);

    const navigate = useNavigate();
    const [Usuario, setUsuario] = useState([]);
    const idUsuario = init()

    useEffect(() => {
        if (idUsuario !== "") {
            setEstadoLogueado(false)
        }
        if ((idUsuario.length === 0) && (idUsuario !== "")) {
            localStorage.setItem("usuario", JSON.stringify(""))
            localStorage.setItem("fotoPerfil", JSON.stringify(""))
            setEstadoLogueado(true)
        }
        cargarData()
    }, []);

    useEffect(async () => {
        const snap = await getDocs(collection(db, "usuarios"))

        snap.forEach((doc) => {
            if (doc.id === idUsuario) {
                localStorage.setItem("fotoPerfil", JSON.stringify(doc.data().foto))
            }
        });
    }, [!estadoLogueado]);

    const cargarData = async () => {
        const snap = await getDocs(collection(db, "usuarios"))
        const usuario = [];

        snap.forEach((doc) => {
            if (doc.id === idUsuario) {
                usuario.push({ ...doc.data() });
            }
        });
        setUsuario(usuario);
    }

    const cerrarSesion = () => {
        localStorage.setItem("usuario", JSON.stringify(""))
        localStorage.setItem("fotoPerfil", JSON.stringify(""))
        navigate('/')
        window.location.replace('');
    }

    const abrirEditarCuenta = () => {
        setModalOpenAccount(true);
    }

    const gestionarPublicaciones = () => {
        navigate('/showPublications/' + idUsuario)

    }

    return (
        <header>
            <div className="Header-div row g-0">
                <div className="Header-icons">
                    <Link to="/"><img src={icono} className="Header-icon" alt="Icono" /></Link>
                    <Link to="/"><img src={logo} className="Header-logo" alt="logo" /></Link>
                </div>
                {estadoLogueado ?
                    (
                        <div className="Header-link row g-0" >
                            <div className="link justify-content-md-center col-xl-8 ">
                                <p className='link' onClick={() => { setModalOpenLogin(true); }} style={{ marginRight: "20%" }}>Iniciar Sesión</p>
                            </div>
                            <div className="link justify-content-md-center col-xl-4">
                                <p className='link' onClick={() => { setModalOpenRegister(true); }} style={{ marginRight: "20%" }}>Registrarme</p>
                            </div>
                        </div>
                    ) : (
                        <div className="Header-link" >
                            {Usuario.map(usuario =>
                                <div key={usuario.nombre} className="dropdown">
                                    <div id="userDiv" className="row g-0" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                                        <div id="userImage" className="justify-content-center col-2 col-md-1">
                                            <img id="imagen" src={usuario.foto} alt="Foto del Usuario" />
                                        </div>
                                    </div>
                                    <div className="dropdown-menu nav-user-dropdown" id="ventanaFlotante">
                                        <div className="nav-user-info">
                                            <div id="userImage2">
                                                <img id="imagen" src={usuario.foto} alt="Foto del Usuario" />
                                            </div>
                                            <h5 id="userName" className="mb-0 text-white nav-user-name"> {usuario.nombre}</h5>
                                            <p id="userMail" className="mb-0 text-white nav-user-name"> {usuario.correo}</p>
                                        </div >
                                        <p id="opcion" className="dropdown-item" href="#" onClick={abrirEditarCuenta}><i className="fas fa-user mr-2" id="icon" ></i> Editar Cuenta</p>
                                        <p id="opcion" className="dropdown-item" href="#" onClick={gestionarPublicaciones}><i class="fa-solid fa-file-signature" mr-2 id="icon"></i> Mis Publicaciones</p>
                                        <p id="opcionSalir" className="dropdown-item" href="#" onClick={cerrarSesion}><i className="fa-solid fa-arrow-right-from-bracket mr-2" id="icon"></i> Cerrar Sesión</p>
                                    </div>
                                </div>
                            )}
                        </div>
                    )}
            </div>

            <Login
                abierto={modalOpenLogin}
                setAbrir={setModalOpenLogin}
                showAlert={showAlert}
                setModalOpenLogin={setModalOpenLogin}
                setEstadoLogueado={setEstadoLogueado}>
            </Login>

            <Register
                abierto={modalOpenRegistrer}
                setModalOpenRegister={setModalOpenRegister}
                showAlertError={showAlertError}>
            </Register>

            <EditAccount
                abierto={modalOpenAccount}
                setModalOpenAccount={setModalOpenAccount}
                Usuario={Usuario}
                idUsuario={idUsuario}>
            </EditAccount>
        </header>
    );
}

export default Header;