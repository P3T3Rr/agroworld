import React, { useState, useEffect } from 'react';
import Slider from './MinComponents/Slider';
import Response from './Response'
import { db, firebase } from "../firebase"
import { getDocs, collection, orderBy, query, onSnapshot } from 'firebase/firestore';
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom"

const styles = {
    title: {
        marginTop: "50px",
        marginBottom: "25px",
        textAlign: "center",
        fontSize: "xx-large",
    },
    body: {
        textAlign: "justify",
        marginLeft: "5%",
        marginRight: "5%"
    },
    divComentario: {
        borderTop: "2px solid",
        marginTop: "40px",
        marginBottom: "50px"
    },
    divUser: {
        display: "flex",
        justifyContent: "center"
    },
    cuerpo: {
        marginTop: "20px"
    },
    userName: {
        marginTop: "auto",
        alignSelf: "center",
        fontWeight: "bold",
        fontSize: "large",
        cursor: 'pointer'
    },
    divFoto: {
        width: "4rem",
        height: "4rem",
        overflow: "hidden",
        marginTop: "15px",
        marginRight: "5px",
        borderRadius: "100%",
        cursor: 'pointer'
    },
    input: {
        width: "100%",
        height: "40px",
    },
    button: {
        width: "100%",
        height: "40px",
        backgroundColor: "#202020",
        color: "#ffffff"
    }
}

const Comments = ({ verPerfil, comentario, idComentario }) => {

    const init = () => {
        const userData = localStorage.getItem("usuario");
        return userData ? JSON.parse(userData) : [];
    }

    const [estadoLogueado, setEstadoLogueado] = useState(false);
    const idUsuario = init();
    const [resp, setResp] = useState();
    const [Respuestas, setRespuestas] = useState([]);
    const [idUsuarioCreador, setIdUsuarioCreador] = useState("");

    const navigate = useNavigate();
    function verPerfil() {
        navigate('/user/' + idUsuarioCreador)
    }

    useEffect(() => {
        if (idUsuario !== "") {
            setEstadoLogueado(true)
        }
        cargarRespuestas()
        cargarUsuarioCreador(comentario[0])

    }, []);

    const id = useParams().id;

    const comentar = async (coment, idComentario) => {
        const comentario = coment
        const calificacion = [{ Calificacion: 100, Usuario: idUsuario }]
        if (comentario !== "") {
            const fecha = firebase.firestore.Timestamp.now().toDate()
            const comment = { Calificacion: calificacion, Comentario: idComentario, Fecha: fecha, Respuesta: coment, Usuario: idUsuario };
            await db.collection("Respuestas").add(comment);
            setResp("")
            cargarRespuestas()

        }
    }

    const onSubmit = (ev) => {
        ev.preventDefault();
        const coment = ev.target.com.value;
        const idComentario = ev.target.com.id
        comentar(coment, idComentario)
        ev.target.com.value = ""
    }

    const cargarRespuestas = async () => {

        const snap = query(collection(db, "Respuestas"), orderBy("Fecha", "desc"));
        const snapUs = await getDocs(collection(db, "usuarios"))

        onSnapshot(snap, (querySnapshot) => {
            const respuesta = [];
            querySnapshot.forEach((doc) => {
                let coment = { ...doc.data() }
                let comentario = coment.Comentario

                if (comentario === idComentario) {
                    snapUs.forEach((docU) => {
                        if (docU.id === coment.Usuario) {
                            respuesta.push([docU.id, coment, { ...docU.data() }, doc.id]);
                        }
                    });
                }
            });
            setRespuestas(respuesta);
        });
    }

    const cargarUsuarioCreador = async (id) => {
        const snap = await getDocs(collection(db, "usuarios"))
        let usuario;
        let idCreador;

        snap.forEach((doc) => {
            if (doc.id === id) {
                usuario = doc.data();
                idCreador = doc.id;
            }
        });
        setIdUsuarioCreador(idCreador)
    }

    return (
        <>
            <div>
                <div style={styles.divComentario} className="row g-0">
                    <div style={styles.divUser} className="row g-0 col-md-2">
                        <div style={styles.divFoto} className="col-6 col-md-12">
                            <img onClick={verPerfil} id='imagen' src={comentario[2].foto} alt="Foto del Usuario" />
                        </div>
                    </div>

                    <div style={styles.cuerpo} className="col-md-10">
                        <p onClick={verPerfil} style={styles.userName}>{comentario[2].nombre}</p>
                        {comentario[1].Comentario}
                        {estadoLogueado ? (
                            <Slider calificaciones={comentario[1].Calificacion} idSlider={comentario[3] + 1} usuario={idUsuario} componente={'C'} />
                        ) : (
                            <div id='desabilitado'>
                                <Slider calificaciones={comentario[1].Calificacion} idSlider={comentario[3] + 1} usuario={idUsuario} componente={'C'} />
                            </div>
                        )}
                        <br></br>

                        {estadoLogueado ? (
                            <form key={comentario[3]} onSubmit={onSubmit}>
                                <div className="row g-3 col-12 col-lg-11">
                                    <div className="col-12 col-md-9">
                                        <input name="com" placeholder=" Respuesta" style={styles.input} type="text" id={comentario[3]} />
                                    </div>
                                    <div className="col-12 col-md-3">
                                        <button style={styles.button} type="submit">Responder</button>
                                    </div>
                                </div>
                            </form>
                        ) : (
                            <div id='desabilitado' className="row g-3 col-12 col-lg-11">
                                <div className="col-12 col-md-9">
                                    <input placeholder=" Respuesta" style={styles.input} value={resp} onChange={(e) => setResp(e.target.value)} type="text" id={comentario[3]} />
                                </div>
                                <div className="col-12 col-md-3">
                                    <button style={styles.button} onClick={comentar}>Responder</button>
                                </div>
                            </div>
                        )}

                        {Respuestas.map((respuesta, index) => (
                            <Response key={comentario[3] + index} respuesta={respuesta}/>
                        ))}
                    </div>
                </div>
            </div>
        </>
    )
}

export default Comments