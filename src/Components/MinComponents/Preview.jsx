import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap';
import { Carousel } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faStarHalfStroke } from '@fortawesome/free-solid-svg-icons'
import Header from '../Header';
import Spinner from './Spinner';

const styles = {
    modal: {
        maxWidth: '70%',
        width: '70%',
        margin: '30px auto'
    },
    closeButton: {
        marginTop: '20px',
        marginRight: '20px'
    },
    title: {
        textAlign: 'center',
        marginTop: '10px',
        fontSize: 'large'
    },
    cuerpo: {
        textAlign: 'justify',
        fontSize: 'middle'
    },
    publicidad: {
        borderLeft: "2px solid black",
        marginLeft: '20px',
    },
    slider: {
        marginLeft: "20px",
        width: "60%"
    },
    divSlider: {
        marginTop: "20px"
    },
    icoSlider: {
        height: "25px",
        color: "#006eff"
    },
    divSliderPromedio: {
        marginTop: "20px"
    },
    icoPromedio: {
        marginLeft: "20px",
        height: "25px"
    },
    divCalificarPublicacion: {
        alignSelf: "center",
        textAlign: "center",
    }
};

const login = ({ Abierto, Cerrar, Publicacion, Imagenes, Publicar, Loading }) => {

    const cerrar = () => {
        Cerrar(false);
    }

    return (
        <div>
            <Modal isOpen={Abierto} style={styles.modal}>
                <div id='desabilitadoVer' >
                    <Header />
                </div>

                <ModalBody style={{ margin: '30px', marginTop: '80px' }}>
                    <div className='row g-0' >
                        <div className="col-9">
                            <div style={styles.title}>
                                <p>{Publicacion.Titulo}</p>
                            </div>
                            <div style={styles.cuerpo}>
                                <p>{Publicacion.Cuerpo}</p>
                            </div>

                            <div className="row g-0">
                                <div className="col-md-6" >
                                    <Carousel style={{ marginBottom: '30px' }}>
                                        {Imagenes.map((image) => (
                                            <Carousel.Item>
                                                <div style={{height: '200px'}}>
                                                    <img id='imagen' src={image.url} alt={image.nombre} />
                                                </div>
                                            </Carousel.Item>
                                        ))}
                                    </Carousel>
                                </div>

                                <div className="col-md-6" style={styles.divCalificarPublicacion}>
                                    <p>¿Qué te pareció la publicación?</p>
                                    <div style={styles.divSlider}>
                                        <FontAwesomeIcon style={styles.icoSlider} icon={faStar} />
                                        <input style={styles.slider} type="range" min="0" max="100" defaultValue="100" />
                                    </div>
                                    <div style={styles.divSliderPromedio}>
                                        <p>Puntaje promedio: 100% <FontAwesomeIcon style={styles.icoPromedio} icon={faStarHalfStroke} /></p>
                                    </div>
                                </div>
                            </div>

                            <div className='row g-2'>
                                <div className='col-12 col-md-6'>
                                    <Button onClick={cerrar} outline color="danger" block>Cancelar</Button>
                                </div>
                                {Loading ? (
                                    <div className='col-12 col-md-6'>
                                        <Button color="primary" block><Spinner texto={"Publicando..."}/></Button>
                                    </div>
                                ) : (
                                    <div className='col-12 col-md-6'>
                                        <Button onClick={Publicar} color="primary" block> Publicar</Button>
                                    </div>
                                )}
                            </div>
                        </div>

                        <div className="col-1" style={styles.publicidad}>
                            <div style={{ marginTop: '10px', margin: 'auto 100%', fontSize: "1.5vw", }}>
                                <p>Publicidad</p>
                            </div>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default login