import React, { useState, useEffect } from 'react';
import {Spinner} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css'
import './styles.css';

function spinner({texto}){
    return(
        <div className='row g-0 justify-content-md-center'>
            <div className='col-12 col-md-1'>
            <Spinner id='spinnerReactstrap'/>
            </div>
            <div className='col-12 col-md-8 col-lg-5'>
                {texto}
            </div>
        </div>
    )
}

export default spinner;