import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faStarHalfStroke } from '@fortawesome/free-solid-svg-icons'
import { db } from "../../firebase"
import { updateDoc, doc } from 'firebase/firestore';

const styles = {

    slider: {
        marginLeft: "20px",
        width: "180px"
    },
    divSlider: {
        marginTop: "20px"
    },
    icoSlider: {
        height: "25px",
        color: "#006eff"
    },
    divSliderPromedio: {
        marginTop: "20px"
    },
    icoPromedio: {
        marginLeft: "20px",
        height: "25px"
    }
}

const Slider = ({ calificaciones, usuario, idSlider, componente }) => {

   
    const [calificacionUsuario, setCalificacionUsuario] = useState(50)
    const [number, setCalificacion] = useState(calificacionUsuario);
    useEffect(() => {
        obtenerCalificacion()
        obtenerUltimaCalificacion()
    }, []);

    const obtenerCalificacion = () => {
        let suma = 0;
        let cali = 0
        calificaciones.forEach(elemento => suma += elemento.Calificacion);
        cali = suma / (calificaciones.length)
        setCalificacion(cali)
    };

    const obtenerUltimaCalificacion = () => {
        calificaciones.forEach(elemento => {
            if (elemento.Usuario === usuario) {
                setCalificacionUsuario(elemento.Calificacion)
            }
        });
    }

    const calificar = async () => {
        let existe = false
        let calificacion = parseInt(document.getElementById(idSlider).value, 10)
        calificaciones.forEach(elemento => {
            if (elemento.Usuario === usuario) {
                elemento.Calificacion = calificacion
                existe = true
            }
        });
        if (!existe) {
            calificaciones.push({ Calificacion: calificacion, Usuario: usuario })
        }

        if (componente === "P"){
            await updateDoc(doc(db, "Publicaciones", idSlider), { Calificacion: calificaciones });
        }
        else if (componente === "C"){
            await updateDoc(doc(db, "Comentarios", idSlider.substring(0, idSlider.length - 1)), { Calificacion: calificaciones });
        }
        else{
            await updateDoc(doc(db, "Respuestas", idSlider), { Calificacion: calificaciones });
        }
        obtenerCalificacion()
    }

    return (
        <>
            <div style={styles.divSlider}>
                <FontAwesomeIcon style={styles.icoSlider} icon={faStar} />
                <input style={styles.slider} id={idSlider} type="range" min="0" max="100" defaultValue={calificacionUsuario} onChange={calificar} />
            </div>
            <div style={styles.divSliderPromedio}>
                <p>Puntaje promedio: {Math.round(number)}% <FontAwesomeIcon style={styles.icoPromedio} icon={faStarHalfStroke} /></p>
            </div>
            </>
    )
}

export default Slider

