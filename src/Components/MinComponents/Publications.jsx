import React, { useEffect } from 'react';
import { Button, Card } from 'react-bootstrap';
import './styles.css';
import { useNavigate } from "react-router-dom"
import { db } from "../../firebase"
import { getDocs, collection, query, orderBy } from 'firebase/firestore';

const Publications = ({ tema, setListaOriginal, listaOriginal, setListAux, listaAux, setListaAuxBuscada, setListaCompleta, vaciar }) => {
    const navigate = useNavigate();
    const publicationCollection = collection(db, "Publicaciones")

    const getProducts = async () => {
        const data = await getDocs(query(publicationCollection, orderBy("Fecha", "desc")))

        setListaOriginal(data.docs.map((doc) => (
            { ...doc.data(), id: doc.id }
        )))
        setListAux(data.docs.map((doc) => (
            { ...doc.data(), id: doc.id }
        )))
        setListaCompleta(data.docs.map((doc) => (
            { ...doc.data(), id: doc.id }
        )))

    }

    useEffect(() => {
        getProducts()
    }, []);

    useEffect(() => {
        const nueva = []
        if (tema !== "Todos los temas") {
            for (let index = 0; index < listaAux.length; index++) {
                if (listaAux[index].Tema === tema) {
                    nueva.push(listaAux[index])
                }
            }
            setListaOriginal(nueva)
            setListaAuxBuscada(nueva)

        } else {
            setListaOriginal(listaAux)
        }
        vaciar()
    }, [tema]);

    return (
        <div className="row g-0">
            {listaOriginal.map(elemento =>
                <div key={elemento.id} className=" col-xl-4 col-md-6" style={{ marginBottom: '2%' }}>
                    <Card style={{ marginLeft: '5%' }}>
                        <Card.Body style={{ height: '280px' }}>
                            <Card.Title style={{ textAlign: 'center' }}>{elemento.Titulo}</Card.Title>
                            <br/>
                            <div id='Card' className='row g-3'>
                                <div className='col-7'>
                                    <Card.Text id='text'>{elemento.Cuerpo}</Card.Text>
                                </div>
                                <div id='divImagesCards' className='col-5'>
                                    <Card.Img id='imagen' variant="top" src={elemento.Imagenes[0]} />
                                </div>
                            </div>
                        </Card.Body>
                        <Card.Footer>
                            <div className="d-grid gap-1">
                                <Button onClick={() => navigate('/publication/' + elemento.id)} variant="primary" size='lg'>Ver Publicación</Button>
                            </div>
                        </Card.Footer>
                    </Card>
                </div>
            )}
        </div>
    )
}

export default Publications