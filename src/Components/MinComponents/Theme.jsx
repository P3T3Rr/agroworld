import React, { useState, useEffect } from 'react';
import { DropdownButton, Dropdown } from 'react-bootstrap';
import { db } from '../../firebase'
import { getDocs, collection } from 'firebase/firestore';

const Theme = ({setTema,tema}) => {

    const [Temas, setLista] = useState([]);
    useEffect(() => {
        cargarData()
    }, []);

    const cargarData = async () => {
        const snap = await getDocs(collection(db, "Temas"))
        const lista = [];

        snap.forEach((doc) => {
            lista.push({ ...doc.data() }.Tema)
        });
        setLista(lista);
    }

    const seleccionarTema = (event) => { 
        setTema(event.target.id)
        window.localStorage.setItem("text", event.target.id)
    }

    return (
        
            <DropdownButton size="lg" className="d-grid gap-1" variant="outline-secondary" title={tema} id="input-group-dropdown-1" >
                {Temas.map(elemento => 
                    <Dropdown.Item key={elemento} id={elemento} onClick={seleccionarTema}>{elemento}</Dropdown.Item>
                )}
            </DropdownButton>
       
    )
}

export default Theme