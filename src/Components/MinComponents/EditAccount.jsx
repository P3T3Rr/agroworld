import { Button, Modal, ModalBody, FormGroup, Input, Label } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark, faPen } from '@fortawesome/free-solid-svg-icons';
import React, { useState } from 'react';
import { firebase, db } from '../../firebase'
import './styles.css'
import { updateDoc, doc } from 'firebase/firestore';


const styles = {
    closeButton: {
        marginTop: '20px',
        marginRight: '20px'
    },
    title: {
        marginLeft: '15%',
        marginRight: '15%',
        textAlign: 'center',
        fontSize: 'x-large'
    },
    modalBody: {
        marginLeft: '15%',
        marginRight: '15%'
    },
    modalFooter: {
        marginLeft: '15%',
        marginRight: '15%',
        marginTop: '10px',
        marginBottom: '70px'
    },
    icoGoogle: {
        maxHeight: '24px',
        marginRight: '20px'
    }
};


function EditAccount({ abierto, setModalOpenAccount, idUsuario, Usuario }) {

    const getFoto = () => {
        const dataFoto = localStorage.getItem("fotoPerfil");
        return dataFoto ? JSON.parse(dataFoto) : [];
    }

    const idFoto = getFoto();
    const [fotoPerfilTemporal, setfotoTemporal] = useState(idFoto);
    const [fotoPerfilPermanente, setfotoPerfilPermanente] = useState();

    const cerrar = () => {
        setModalOpenAccount(false);
        setfotoTemporal(idFoto)
    }

    const changeInput = (e) => {
        let url = URL.createObjectURL(e.target.files[0])
        setfotoTemporal(url);
        setfotoPerfilPermanente(e.target.files[0])
    }

    const updateData = async () => {

        var urlFoto = fotoPerfilPermanente;
        let nombre = document.getElementById("nombre").value
        let link = document.getElementById("link").value

        if (urlFoto !== undefined){
            try {
                const archivo = fotoPerfilPermanente;
                const storageRef = firebase.storage().ref('Usuarios');
                const archivoPath = storageRef.child(archivo.name);
                await archivoPath.put(archivo)
                urlFoto = await archivoPath.getDownloadURL()
            } catch (err) {
                console.error(); // Evita que el error aparezca en consola
            }
        }

        Usuario.map(usuario => {
            if (urlFoto === undefined) {
                urlFoto = usuario.foto
            }
            if (nombre === ''){
                nombre = usuario.nombre
            }
            return null
        })

        localStorage.setItem("fotoPerfil", JSON.stringify(urlFoto))
        await updateDoc(doc(db, "usuarios", idUsuario), { foto: urlFoto, nombre: nombre, Link: link });

        window.location.replace('');
    }

    return (
        <div>
            <Modal isOpen={abierto}>
                <div className='closeButton' style={styles.closeButton}>
                    <Button onClick={cerrar} color="danger" size="lg"><FontAwesomeIcon icon={faXmark} /></Button>
                </div>

                <div style={styles.title}>
                    <p>Editar Cuenta</p>
                </div>

                <div id='divFoto' className="col-12 col-lg-1">
                    <img id='imagen' src={fotoPerfilTemporal} alt="Foto del Usuario" />
                </div>
                <label id='botonCargarFoto' className="btn btn-dark">
                    <span style={{ marginRight: "10px" }}> Cambiar foto </span><FontAwesomeIcon icon={faPen} />
                    <input id='inputFoto' hidden type="file" accept='image/*' onChange={changeInput}></input>
                </label>

                {Usuario.map(usuario =>
                    <div key={usuario.nombre}>
                        <ModalBody style={styles.modalBody}>
                            <FormGroup>
                                <Label for="nombre">Nombre Actual</Label>
                                <Input type="text" id="nombre" placeholder={usuario.nombre} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="emailRegister">Email</Label>
                                <Input disabled id="emailRegister" placeholder={usuario.correo} />
                            </FormGroup>
                            <FormGroup>
                                <Label for="link">Sitio Web (Opcional)</Label>
                                <Input id="link" defaultValue={usuario.Link} />
                            </FormGroup>
                        </ModalBody>
                    </div>
                )}

                <div style={styles.modalFooter}>
                    <div>
                        <Button id='buttonRegistrarse' color="primary" onClick={updateData} block> Acutializar Datos</Button>
                    </div>
                    <div style={{ marginTop: '5px' }}>
                    </div>
                </div>
            </Modal>
        </div>
    )

}

export default EditAccount;