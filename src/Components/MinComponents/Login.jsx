import React from 'react'
import Swal from 'sweetalert2'
import { Button, Modal, ModalBody, FormGroup, Input, Label } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import icoGoogle from '../../Imagenes/google.png';
import { firebase, db } from "../../firebase"
import { getFirestore, doc, setDoc, collection, getDocs } from "firebase/firestore";
import { signInWithPopup, getAuth, GoogleAuthProvider } from "firebase/auth";

const styles = {
    closeButton: {
        marginTop: '20px',
        marginRight: '20px'
    },
    title: {
        marginLeft: '15%',
        marginRight: '15%',
        textAlign: 'center',
        fontSize: 'x-large'
    },
    modalBody: {
        marginLeft: '15%',
        marginRight: '15%'
    },
    modalFooter: {
        marginLeft: '15%',
        marginRight: '15%',
        marginTop: '10px',
        marginBottom: '70px'
    },
    icoGoogle: {
        maxHeight: '24px',
        marginRight: '20px'
    }
};

const login = ({ abierto, setAbrir, showAlert, setModalOpenLogin, setEstadoLogueado }) => {

    const cerrar = () => {
        setAbrir(false);
    }

    let existe = false
    let foto = ""

    const Buscar = async (id) => {
        const snap = await getDocs(collection(db, "usuarios"))
        snap.forEach((doc) => {
            if (doc.id === id) {
                existe =  true
                foto = doc.data().foto
            }
        });
    }

    var userData;

    async function registerGoogle() {
        const firestore = getFirestore();
        const provider = new GoogleAuthProvider();
        const auth = getAuth();
        
        await signInWithPopup(auth, provider)
            .then(async (result) => {

                const docuRef = doc(firestore, `usuarios/${result.user.uid}`);

                await Buscar(result.user.uid)

                if (!existe) {
                    await setDoc(docuRef, { correo: result.user.email, nombre: result.user.displayName, foto: result.user.photoURL, Link: "" });
                    localStorage.setItem("fotoPerfil", JSON.stringify(result.user.photoURL))
                }
                else{
                    localStorage.setItem("fotoPerfil", JSON.stringify(foto))
                }

                localStorage.setItem("usuario", JSON.stringify(result.user.uid))

                setEstadoLogueado(false);
                setModalOpenLogin(false);
                window.location.replace('');

            }).catch((error) => {
                console.log(error)
            });
    }

    const login = async () => {
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        let dataFoto;

        try {
            await firebase.auth().signInWithEmailAndPassword(email, password)
                .then(async(result) => {
                    if (result.user.emailVerified === false) {
                        showAlert();
                    }
                    if (result.user.emailVerified === true) {
                        userData = result.user.uid;
                        dataFoto = result.user.photoURL;
                        localStorage.setItem("usuario", JSON.stringify(userData))
                        await Buscar(userData)
                        localStorage.setItem("fotoPerfil", JSON.stringify(foto))

                        setEstadoLogueado(false);
                        setModalOpenLogin(false);
                        window.location.replace('');
                    }
                }).catch((err) => {
                    Swal.fire({
                        title: "Correo y/o contraseña incorrecta",
                        icon: "error",
                        confirmButtonColor: '#4285F4'
                    })
                })
        }
        catch (error) {
            Swal.fire({
                title: "Correo y/o contraseña incorrecta",
                icon: "error",
                confirmButtonColor: '#4285F4'
            })
        }
    };

    return (
        <div>
            <Modal isOpen={abierto} >
                <div className='closeButton' style={styles.closeButton}>
                    <Button onClick={cerrar} color="danger" size="lg"><FontAwesomeIcon icon={faXmark} /></Button>
                </div>
                <div style={styles.title}>
                    <p>Iniciar Sesión</p>
                </div>
                <ModalBody style={styles.modalBody}>
                    <FormGroup>
                        <Label for="usuario">Email</Label>
                        <Input type="text" id="email" placeholder='Correo Electrónico' />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Contraseña</Label>
                        <Input type="password" id="password" placeholder='Contraseña' />
                    </FormGroup>
                </ModalBody>
                <div style={styles.modalFooter}>
                    <div>
                        <Button id='buttonIniciar' color="primary" onClick={login} block> Iniciar Sesión</Button>
                    </div>
                    <div style={{ marginTop: '20px' }}>
                        <Button id='buttonIniciarGoogle' color="outline-dark" block onClick={registerGoogle}>
                            <img src={icoGoogle} alt="google" style={styles.icoGoogle} />
                            Iniciar con Google
                        </Button>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default login