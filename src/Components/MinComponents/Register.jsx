import React, { useState } from 'react';
import { Button, Modal, ModalBody, FormGroup, Input, Label } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark, faPen } from '@fortawesome/free-solid-svg-icons';
import './styles.css'
import { firebase } from '../../firebase'
import { getFirestore, doc, setDoc } from "firebase/firestore";
import { getAuth, sendEmailVerification } from "firebase/auth";
import Swal from 'sweetalert2'
import fotoPerfilPredeterminada from '../../Imagenes/perfil.png';


const styles = {
    closeButton: {
        marginTop: '20px',
        marginRight: '20px'
    },
    title: {
        marginLeft: '15%',
        marginRight: '15%',
        textAlign: 'center',
        fontSize: 'x-large'
    },
    modalBody: {
        marginLeft: '15%',
        marginRight: '15%'
    },
    modalFooter: {
        marginLeft: '15%',
        marginRight: '15%',
        marginTop: '10px',
        marginBottom: '70px'
    },
    icoGoogle: {
        maxHeight: '24px',
        marginRight: '20px'
    }
};

function Register({ abierto, setModalOpenRegister, showAlertError }) {

    const [fotoPerfilTemporal, setfotoTemporal] = useState(fotoPerfilPredeterminada);
    const [fotoPerfilPermanente, setfotoPerfilPermanente] = useState();

    const cerrar = () => {
        setModalOpenRegister(false);
        setfotoTemporal(fotoPerfilPredeterminada)
    }

    const showAlertRegister = () => {
        Swal.fire({
            title: "Cuenta creada con exito",
            text: "Se ha enviado un email, debe derificar la cuenta",
            icon: "success",
            confirmButtonColor: '#4285F4'
        })
    };

    const verificar = () => {
        const auth = getAuth();
        sendEmailVerification(auth.currentUser)
            .then(() => {
                // Email verification sent!
                // ...
            });
    }

    const register = async () => {
        var registerEmail = document.getElementById("emailRegister").value
        var registerPassword = document.getElementById("passwordRegister").value
        var registerPassword2 = document.getElementById("passwordRegister2").value
        var registerName = document.getElementById("nombre").value
        var url = fotoPerfilPredeterminada;

        try {
            const archivo = fotoPerfilPermanente;
            const storageRef = firebase.storage().ref('Usuarios');
            const archivoPath = storageRef.child(archivo.name);
            await archivoPath.put(archivo)
            url = await archivoPath.getDownloadURL()

        } catch (err) {
            console.error(); 
        }

        const firestore = getFirestore();
        try {

            if (registerEmail !== "" && registerPassword !== "" && registerName !== "") {
                if (registerPassword === registerPassword2) {
                    let { user } = await firebase.auth().createUserWithEmailAndPassword(registerEmail, registerPassword)
                    await user.updateProfile({ displayName: registerName, photoURL: url })
                    const docuRef = doc(firestore, `usuarios/${user.uid}`);
                    setDoc(docuRef, { correo: registerEmail, nombre: registerName, foto: url, Link: "" });
                    showAlertRegister();
                    verificar();
                    setModalOpenRegister(false)
                }
                else {
                    showAlertError("contraseñas");
                }
            }
            else {
                showAlertError("campos vacios");
            }
        } catch (error) {
            showAlertError(error.message);
        }
    };

    const changeInput = async (e) => {
        let url = URL.createObjectURL(e.target.files[0])
        setfotoTemporal(url);
        setfotoPerfilPermanente(e.target.files[0])
    }

    return (
        <div>
            <Modal isOpen={abierto}>
                <div className='closeButton' style={styles.closeButton}>
                    <Button onClick={cerrar} color="danger" size="lg"><FontAwesomeIcon icon={faXmark} /></Button>
                </div>

                <div style={styles.title}>
                    <p>Registrarse</p>
                </div>

                <div id='divFoto' className="col-12 col-lg-1">
                    <img id='imagen' src={fotoPerfilTemporal} alt="Foto del Usuario" />
                </div>
                <div id='divBoton' className='row g-0'>
                    <div className='col-2'>
                    </div>
                    <div className='row g-0 col-10'>
                        <div id='divBotonCargarFoto' className='col-9'>
                            <label id='botonCargarFoto' className="btn btn-dark">
                                <span style={{ marginRight: "10px" }}> Cargar foto </span><FontAwesomeIcon icon={faPen} />
                                <input hidden type="file" accept='image/*' onChange={changeInput}></input>
                            </label>
                        </div>
                        <div id='textoOpcional' className='col-1'>
                            <div>(Opcional)</div>
                        </div>
                    </div>
                </div>

                <ModalBody style={styles.modalBody}>
                    <FormGroup>
                        <Label for="nombre">Nombre</Label>
                        <Input type="text" id="nombre" placeholder='Nombre de usuario' />
                    </FormGroup>
                    <FormGroup>
                        <Label for="usuario">Email</Label>
                        <Input type="text" id="emailRegister" placeholder='Correo Electrónico' />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Contraseña</Label>
                        <Input type="password" id="passwordRegister" placeholder='Contraseña' />
                    </FormGroup>
                    <FormGroup>
                        <Label for="password">Confirmar Contraseña</Label>
                        <Input type="password" id="passwordRegister2" placeholder='Contraseña' />
                    </FormGroup>
                </ModalBody>
                <div style={styles.modalFooter}>
                    <div>
                        <Button id='buttonRegistrarse' color="primary" onClick={register} block> Registrarse</Button>
                    </div>
                    <div style={{ marginTop: '20px' }}>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default Register