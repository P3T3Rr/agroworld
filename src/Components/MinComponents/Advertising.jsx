import React, { useState, useEffect } from 'react';
import { db } from "../../firebase"
import { getDocs, collection } from 'firebase/firestore';

const styles = {
    title: {
        marginTop: "50px",
        marginBottom: "25px",
        textAlign: "center",
        fontSize: "2vw",

    },
    divPublicidad: {
        textAlign: "center",
        maxHeight: "210px",
    },
    divImagen: {
        marginLeft: "8%",
        marginRight: "8%"
    },
    publicidad: {
        marginBottom: "20px",
        width: "100%",
        height: "auto"
    }
}

const Advertising = () => {

    const [publicidad, setPublicidad] = useState([]);

    useEffect(() => {
        cargarData()
    }, []);

    const cargarData = async () => {
        const snap = await getDocs(collection(db, "Publicidad"))
        const publicidad = [];

        snap.forEach((doc) => {
                publicidad.push({ ...doc.data() });          
        });
        setPublicidad(publicidad);
    }

    return (
        <div>
            <p style={styles.title}>Publicidad</p>
            <div style={styles.divPublicidad}>
                <div style={styles.divImagen}>
                    {publicidad.map((image) => (
                        <img key={image} style={styles.publicidad} src={image.direccion} alt="publicidad" />
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Advertising